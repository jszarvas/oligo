### PCR oligo checking script for viral sequences

These scripts are for in silico testing of PCR oligos against entries in the KVIT database (https://bitbucket.org/genomicepidemiology/kvit_db).  
It calculates the number of mismatches between each probe and viral sequence based on local alignment scores.  
The single process version is platform independent.

###### Dependencies
```
Python 3.6+
Biopython 1.71+
```

###### Installation

Download repository under _Downloads_ menu on the left, then unzip at desired location.  

With git:
```
# Go to desired folder
cd /path/to/install_dir
# Clone master branch
git clone https://bitbucket.org/jszarvas/oligo.git
```

###### Usage

Either by running primer_check.py or through oligo_wrapper.py (for Windows).  
The script creates an oligo.pic file in the script directory, where it stores the information about the previously checked oligo - genbank entry pairs, and the mismatch statistics. Therefore after the first run, it only checks entries and oligos that hasn't been aligned against each other before.  
The script runs 3 parallel processes by default, this can be modified by editing the value of the n_jobs variable.

_Input_  
The primer table input (-p) has to be a semicolon separated file (csv exported from Excel), with a header and the following columns:  
*ID*: unique identifier of the oligo  
*Sequence*: 5'->3' sequence, containing only IUPAC DNA characters  
*Type*: FWD (forward), PROBE (probe), REV (reverse)
*Target_taxid*: the NCBI taxonomical id (txid) of the target organism(s), highest category

Example (from https://doi.org/10.1016/j.jviromet.2013.08.039):
```
ID;Sequence;Type;Target_taxid
UsuFP (Nikolay fwd);CAAAGCTGGACAGACATCCCTTAC;FWD;64286
UsuP (Nikolay probe);AAGACATATGGTGTGGAAGCCTGATAGGCA;PROBE;64286
UsuRP (Nikolay rev);CGTAGATGTTTTCAGCCCACGT;USUV-N;64286
```

_Command_  
```
$ python3 primer_check.py -h
usage: primer_check.py [-h] [-p PROBE_P] [-k KVIT_P] [-o OUT]

Primer validation

optional arguments:
  -h, --help            show this help message and exit
  -p PROBE_P, --probes PROBE_P
                        Primer table
  -k KVIT_P, --kvit KVIT_P
                        KVITdb path
  -o OUT, --out OUT     Output path
```
Example of use on Unix with the output directory defined:
```
/path/to/install_dir/primer_check.py -p <primers>.csv \
-k /path/to/kvitdb -o /path/to/output_folder
```

_Output_  
An html file of the summary statistics of all oligos (oligos_date.html). In addition, there is a file with the output of the raw alignments and scores (oligos_aln_date_time.txt), and one with the summary of all alignments for a given oligo (oligos_aln_date_time.stats.txt).

###### License  
Copyright (c) 2019, Judit Szarvas, Technical University of Denmark. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
You may obtain a copy of the License at
	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
See the License for the specific language governing permissions and
limitations under the License.
