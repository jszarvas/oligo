#!/usr/bin/env python3

import os
import sys
import subprocess
import shlex

## PARAMETERS TO CHANGE
primer_file = "PATH TO PRIMER FILE"
kvitdb_folder = "FOLDER FOR KVIT"
output_folder = "FOLDER FOR OUTPUT"

## SCRIPT STARTS HERE
script_dir = os.path.dirname(os.path.realpath(__file__))
primer_check = os.path.join(script_dir, "primer_check_single.py")

if not os.path.exists(primer_file):
    sys.exit("Oligo file is needed.")
if not os.path.exists(kvitdb_folder) and not os.path.isdir(kvitdb_folder):
    sys.exit("Kvitdb path is needed.")

cmd = "{} {} -p {} -k {}".format(sys.executable, primer_check, os.path.realpath(primer_file), os.path.realpath(kvitdb_folder))
if os.path.exists(output_folder):
    cmd += " -o {}".format(output_folder)
if sys.platform.startswith('win32'):
    # Windows
    p = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
else:
    # Unix
    p = subprocess.run(shlex.split(cmd), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
print(p.stdout.decode('utf-8'))
