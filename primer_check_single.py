#!/usr/bin/env python3

import os
import sys
import time
import argparse
from datetime import datetime
from urllib.request import urlopen,URLError
from urllib.parse import urlencode,quote
import json
try:
    import cPickle as pickle
except ImportError:
    import pickle
from Bio import SeqIO
from Bio import pairwise2
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio.Data.IUPACData import ambiguous_dna_values

def warning(m):
    print(m, file=sys.stderr)

def safe_urlopen(url, params):
    tries = 2
    while tries != 0:
        try:
            stream = urlopen(url, urlencode(params, doseq=False, safe='', encoding="ascii", errors=None, quote_via=quote).encode("ascii"))
            return stream
        except URLError as e:
            tries -= 1
            time.sleep(2)
    return None

def score_oligo(fasta, aln_file):
    score = int(pairwise2.align.localms(fasta.seq, query, pair,mis,gap,ext,score_only=True))
    aln_str = ""
    aln_len = 0
    qual = "PASS"
    gaps = 0
    mm = None
    aln_str = None
    if score < maxScore:
        # possibly due to ambiguity
        # go through the ambiguous bases and check the corresponding nucleotide in the sequence
        variant = query
        for pos in amb_pos:
            for base in ambiguous_dna_values[query[pos]]:
                variant = str(variant)[:pos] + base + str(variant)[pos + 1:]
                vscore = int(pairwise2.align.localms(fasta.seq, variant, pair,mis,gap,ext,score_only=True))
                if vscore > score:
                    score = vscore
                    break
        if score != maxScore:
            qual = "FAIL"
            aln = pairwise2.align.localms(fasta.seq, variant, pair,mis,gap,ext,one_alignment_only=True)
            aln_str = pairwise2.format_alignment(*aln[0])
            gaps = aln_str.count("-")
            #aln len on probe
            aln_len = len(aln_str.split("\n")[2].split(" ")[-1])
            if (aln_len - gaps) + 1 < len(query):
                qual = "SHORT"
                print(" ".join([aln_str[:-1], fasta.id]), "\t".join([fasta.id, entry[1], str(maxScore), str(score),str(mm), str(gaps), qual]), sep="\n", file=aln_file, flush=True)
                return None
    mm = min(int((maxScore - score) / (pair - mis)),6)
    mm_det = []
    if aln_str is None:
        print(fasta.id, entry[1], maxScore, score, mm, gaps, qual, sep="\t", file=aln_file, flush=True)
    else:
        print(" ".join([aln_str[:-1], fasta.id]), "\t".join([fasta.id, entry[1], str(maxScore), str(score),str(mm), str(gaps), qual]), sep="\n", file=aln_file, flush=True)
        if not gaps:
            b = 0
            mm_det = []
            target = aln_str.split("\n")[0].strip().split(" ")[-1]
            markers = aln_str.split("\n")[1].strip()
            # one end of alignment is missing
            if aln_len < len(query):
                aln_q = aln_str.split("\n")[2].split(" ")[-1]
                if aln_q[:3] != variant[:3]:
                    # beginning missing
                    mm_det.append([0, "N"])
                    b = 1
                elif aln_q[-3:] != variant[-3:]:
                    # end missing
                    mm_det.append([len(query)-1, "N"])
            start = 0
            r = markers.find(".", start)
            # find all mismatches
            while r > -1:
                mm_det.append([r + b, target[r]])
                start = r + 1
                r = markers.find(".", start)
    return [mm, mm_det]

def html_print(data_str, js_str, css_str, html_filename):
    header = """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
  <title>Primer check output</title>
  {javascript}
  {css}
</head>
<body>"""

    body_str = "\n".join(['  <h3>Primer check output</h3>',
                        '  <h4>Date: {}</h4>'.format(datetime.today().strftime("%Y-%m-%d")),
                        '  <input type="text" id="myInput" onkeyup="groupSearch()" placeholder="Search for family/genus">'])

    footer = "</body>\n</html>\n"

    #<script type="text/javascript" src="templates.json"></script>
    #<link rel="stylesheet" type="text/css" href="primer.css">
    html_cont = [header.format(javascript=js_str, css=css_str)]
    html_cont.append(body_str)
    html_cont.append(data_str)
    html_cont.append(footer)

    with open(html_filename, "w") as op:
        op.write("\n".join(html_cont))

def create_tables(raw_data):
    table_str = """  <table id="oligoTable">
    <tr class="header">
{header}
    </tr>
    {row}
  </table>"""

    header = []
    for item in ["Family", "OliNaam", "Type", "0", "1", "2", "3", "4", "5", ">5", "Total"]:
        header.append("      <th>{}</th>".format(item))

    row_unit = "      <tr>\n{cell}\n      </tr>"
    cell_unit = "        <td>{}</td>"
    rows = []
    for oligo in raw_data.keys():
        cells = []
        cells.append(cell_unit.format(raw_data[oligo]['family']))
        cells.append(cell_unit.format(oligo))
        cells.append(cell_unit.format(raw_data[oligo]['type']))
        for val in raw_data[oligo]['stats']:
            cells.append(cell_unit.format(val))
        cells.append(cell_unit.format(len(raw_data[oligo]['records'])))
        rows.append(row_unit.format(cell="\n".join(cells)))

    table = table_str.format(header="\n".join(header), row="\n".join(rows))
    return table



parser = argparse.ArgumentParser(description='Primer validation')
parser.add_argument("-p", "--probes", dest="probe_p", help="Primer table")
parser.add_argument("-k", "--kvit", dest="kvit_p", help="KVITdb path")
parser.add_argument("-o", "--out", dest="out", help="Output path")
args = parser.parse_args()

n_jobs = 3

ambiguous_dna_letters = 'RYWSMKHBVDN'

# alignment scores:
pair = 3
mis = -2
gap = -5
ext = -2

# get KVITdb overview summary and extract the entries for each taxid
overview = {}
try:
    kfile = open(os.path.join(args.kvit_p, "database_overview.txt"), "r")
    _ = kfile.readline()
    for line in kfile:
        #Accession no.	Organism	Taxanomy id	Family
        tmp = line.strip().split("\t")
        # taxid: full record
        if tmp[2] not in overview:
            overview[tmp[2]] = [tmp]
        else:
            overview[tmp[2]].append(tmp)
    kfile.close()
except Exception as e:
    sys.exit("KVIT overview")

# pickles for checked oligos and stats
script_dir = os.path.dirname(os.path.realpath(__file__))
# oligo: {family: "", type: "", records: [], stats: []}
checked_oligos = {}
if os.path.exists(os.path.join(script_dir, "oligos.pic")):
    with open(os.path.join(script_dir, "oligos.pic"), "rb") as fp:
        checked_oligos = pickle.load(fp)

try:
    pfile = open(args.probe_p, "r")
except Exception as e:
    sys.exit("Probes")

probes = []
# oliNaam	oliSequentie	type	target_taxid
_ = pfile.readline()
for line in pfile:
    probes.append(line.strip().split(";"))
pfile.close()

txt_name = "oligos_aln_{}.txt".format(datetime.now().strftime("%Y_%m_%d_%H_%M"))
oligo_det_name = "oligos_aln_{}.stats.txt".format(datetime.now().strftime("%Y_%m_%d_%H_%M"))
if args.out is not None:
    txt_name = os.path.join(args.out, txt_name)
    oligo_det_name =  os.path.join(args.out, oligo_det_name)
try:
    oligo_det_file = open(oligo_det_name, "w")
    aln_file = open(txt_name, "w")
except Exception as e:
    sys.exit("Aln output")

# sort by taxid
sorted(probes, key=lambda probe: probe[3])
prev_taxid = ""
family_records = None
for entry in probes:
    taxid = entry[3]
    if taxid != prev_taxid:
        # collect KVIT records for taxid and below in subtree
        prev_taxid = taxid
        kvit_records = []
        family_records = []
        folder = None

        # https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=taxonomy&term=txid12637[Subtree]&retmode=json
        query = "txid{}[Subtree]".format(taxid)
        params = {
            'db': 'taxonomy',
            'term': query,
            'retmax': '10000',
            'retmode': 'json'
        }
        url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi'
        jsonrecord = safe_urlopen(url, params)
        if jsonrecord is None:
            warning("Json file not received from Entrez for {}".format(taxid))
            continue

        json_cont = json.load(jsonrecord)
        if json_cont['esearchresult'].get('count') is None or json_cont['esearchresult']['count'] == "0":
            warning("No record found for {}".format(taxid))
            continue
        else:
            for tix in json_cont['esearchresult']['idlist']:
                if tix in overview:
                    # assumes that probe is for one family/genus
                    if folder is None:
                        folder = overview[tix][0][-1]
                    kvit_records.extend([x[0] for x in overview[tix]])

            if folder is not None:
                fasta_path = os.path.join(args.kvit_p, folder, "{}.fsa".format(folder))
                if not os.path.exists(fasta_path):
                    continue
                genoms = SeqIO.parse(fasta_path, "fasta")
                for rec in genoms:
                    if rec.id.split(":")[0] in kvit_records:
                        family_records.append(rec)

    # check probe against the kvit_records
    if family_records:
        # init the oligo in checked_oligos with oliNaam
        if entry[0] not in checked_oligos:
            checked_oligos[entry[0]] = { 'family': folder,
                                         'type': entry[2],
                                         'records': [],
                                         'stats': [0,0,0,0,0,0,0] }

        # process probe sequence depending on type entry[2]
        pseq = Seq(entry[1].upper(), IUPAC.ambiguous_dna)
        query = None
        if entry[2] == "REV":
            query = pseq.reverse_complement()
        else:
            # FWD and PROBE
            query = pseq

        amb_pos = []
        maxScore = len(query) * pair
        for i in range(len(query)):
            if query[i] in ambiguous_dna_letters:
                amb_pos.append(i)
        expected = ((len(query) - len(amb_pos)) * pair) + (mis * len(amb_pos))

        # zero, 1, 2,3,4,5, >5
        mismatches = checked_oligos[entry[0]]['stats']

        working_list = []
        for r in family_records:
            if r.id.split(":")[0] not in checked_oligos[entry[0]]['records']:
                checked_oligos[entry[0]]['records'].append(r.id.split(":")[0])
                working_list.append(r)

        snps = {'A': [], 'C': [], 'G': [], 'T': [], 'N': [],}
        for fasta_entry in  working_list:
            res = score_oligo(fasta_entry, aln_file)
            if res is not None:
                mismatches[res[0]] += 1
                for nuc in res[1]:
                    snps[nuc[1]].append(nuc[0])

        if snps:
            print(entry[0], entry[2], entry[1], checked_oligos[entry[0]]['stats'], file=oligo_det_file)
            print("#\t", "\t".join(str(query)), file=oligo_det_file)
            for nuc in ["A", "C", "G", "T", "N"]:
                print(nuc, end="\t", file=oligo_det_file)
                for i in range(len(query)):
                    if snps.get(nuc):
                        print(snps.get(nuc).count(i), end="\t", file=oligo_det_file)
                    else:
                        print("0", end="\t", file=oligo_det_file)
                print("", file=oligo_det_file, flush=True)
        print(entry[0], checked_oligos[entry[0]]['records'][0], checked_oligos[entry[0]]['records'][-1], checked_oligos[entry[0]]['stats'])

aln_file.close()
oligo_det_file.close()
with open(os.path.join(script_dir, "oligos.pic"), "wb") as fp:
    pickle.dump(checked_oligos, fp)

html_name = "oligos_{}.html".format(datetime.today().strftime("%Y_%m_%d"))
if args.out is not None:
    html_name = os.path.join(args.out, html_name)
    # copy the css and js files over too
    try:
        import shutil
        if not os.path.exists(os.path.join(args.out, "primer.css")):
            shutil.copy(os.path.join(script_dir, "primer.css"), args.out)
        if not os.path.exists(os.path.join(args.out, "primercheck.js")):
            shutil.copy(os.path.join(script_dir, "primercheck.js"), args.out)
    except:
        warning("Css and/or javascript files need to be manually copied over to output folder")

t = create_tables(checked_oligos)
js_str = '<script type="text/javascript" src="primercheck.js"></script>'
css_str = '<link rel="stylesheet" type="text/css" href="primer.css">'
html_print(t, js_str, css_str, html_name)

sys.exit()
